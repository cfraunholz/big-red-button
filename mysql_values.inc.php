<?php
// The Big Red Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8
$first = false;
$code = '';
$code .= '$sql = "INSERT INTO ' . (($tableName)?$tableName:'tableName') . (($_REQUEST['mysql_full']) ? '(' . $blankList . ')':'') . ' VALUES(';
foreach ($array as $key => $value) {
	if ($first) $code .= ',' . chr(13);
	if ($simpleType[$key] != 'int' && $simpleType[$key] != 'ckb') $code .= '\'" . mysql_real_escape_string(';
	else {
		$code .= (!$first) ? '"
    .':'" . ';
		if ($_REQUEST['mysql_int_typecast']) $code .= ' (int) ';
	}
	if (WEBSITE == 'HROSE')
            $method = 'validdb';
        else
            $method = $_REQUEST['type'] == 'form' ? 'varname' : null;
        $code .= varname($value, $method);
	if ($simpleType[$key] != 'int' AND $simpleType[$key] != 'ckb') $code .= ')
    . "\'';
	else $code .= (!$first) ? '
    . "' : '
    . "';
	$first = true;
}
$code .= (!$first) ? '"' : '';
if (strpos($code, '
. ""', strlen($code) - 5) !== false) {
	$code = substr($code, 0, -4);
}
$code .= ')"';
$mysqlValuesCode = $code;