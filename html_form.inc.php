<?php
// The Big Red Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8
$first = false;
$escapeMethod = 'sss';
$sep = '';


/***** Select *****/
$initCode = '$modul="'.$tableName.'";

require("inc/req.php");

// For Administrators only
GRGR(1000);
';


/***** Select *****/
$code = '';

$pkfirst = false;
$pkCheckCode = '';
$pkSelectCode = '';
foreach ($primary as $key => $pk) {
	if ($pkfirst) {
		$pkCheckCode .= ' AND ';
		$pkSelectCode .= (($pkLast == 'int') ? ' . "' : '') . ' AND ';
	}
	$pkCheckCode .= varname($pk);
	$pkSelectCode .= varname($pk, 'blank') . ' = ';
	if (in_array($simpleType[$key], array('int', 'ckb'))) {
		$pkSelectCode .= '" . (int) ' . varname($pk);
		$pkLast = 'int';
	} else {
		$pkSelectCode .= '\'" . ' . varname($pk) . ' . "\'"';
	}
	$pkfirst = true;
}

$code .= '
} else if (' . $pkCheckCode . ') {
	$sql = "SELECT * FROM '.$tableName.' WHERE ' . $pkSelectCode . ';
	$data = mysql_fetch_assoc(mysql_query($sql));
    foreach ($data as $key => $value) {
        '.((WEBSITE == 'HROSE') ?'$_VALID[$key] = $value;':'
        $$key = $value;').'
    }
}
// manuelle Eingabe überschreibt DB-Werte
if (isset($_REQUEST[\'submitted\'])) {
    foreach ($_VALID as $key => $value) {
        '.((WEBSITE == 'HROSE') ?'$_VALID[$key] = $value;':
        '$$key = $value;').'
    }
}
';

$formSelectCode = $code;


/***** Save *****/
$code = '';

$code = '
if ('.((WEBSITE == 'HROSE')?'':varname('submitted') . ' AND ') . '!$error) {
	' . str_replace('INSERT', 'REPLACE', $mysqlValuesCode) . ';
    mysql_query($sql) or die(\'DB Insert Error\');
    header(\'Location: '.$tableName.'.php?ok=Done\');
    exit;
}
';

$formSaveCode = $code;


/***** Form *****/
$code = ((WEBSITE == 'HROSE')?'$n4a[\''.$tableName.'.php\'] = ss(\'Back to List\');
require("inc/header.inc.php");':'').'
';
if ($_REQUEST['form_full']) {
	$formMethod = (WEBSITE == 'HROSE')?'valid':'varname';
	if (!$_REQUEST['form_field_errors']) {
            if (WEBSITE == 'HROSE') $code .= '
if ($error) {
	$headerError = implode(\'<br>\', $error);
}'; else
    $code .= '
if ($error) {
	echo \'<p class="error">\' . implode(\'<br>\', $error) . \'</p>\';
}';
	}
}

$code .= '
?>';

if ($_REQUEST['form_full']) $code .= '
<a href="javascript:void(0)" onClick="window.location.href = \''.$tableName.'.php\'"><img alt="<?php sss(\'Back to List\')?>" title="<?php sss(\'Back to List\')?>" src="css/icon/align_just_icon&16.png" class="listmenuicon"></a><br><br>
';
if ($_REQUEST['list_paginator']) 
    $code .= '<?php
/*** Pagination ***/
if ($id) {
    $pageResult = getMemCache($_SESSION[$modul][\'sql\']);
    $prevEntry = $pageResult[$_VALID[\'i\']-1];
    if ($prevEntry) {
        echo \'&nbsp;&nbsp;<a href="\'.$modul.\'_d.php?i=\'.($_VALID[\'i\']-1).\'&amp;id=\'.$prevEntry[$modul.\'_id\'].\'"><img src="css/icon/br_prev_icon&16.png" title="'.trans('Previous').'"></a>\';
    } else {
        echo \'&nbsp;<span style="margin:8px">&nbsp;</span>\';
    }

    $nextEntry = $pageResult[$_VALID[\'i\']+1];
    if ($nextEntry) {
        echo \'&nbsp;&nbsp;<a href="\'.$modul.\'_d.php?i=\'.($_VALID[\'i\']+1).\'&amp;id=\'.$nextEntry[$modul.\'_id\'].\'"><img src="css/icon/br_next_icon&16.png" title="'.trans('Next').'"></a>\';
    }
}?>';
$code .= '
<div class="contentheadline">'.ucfirst($tableName).'</div>
<br>
<div class="contenttext">';

$code .= '
<form id="form'.$tableName.'" name="form'.$tableName.'" method="post" class="formLayout">
<?php if($id) {
  echo \'<input type="hidden" name="id" value="\'.$id.\'">\';
}?>
';

foreach ($array as $key => $value) {
        if ($key != $pk) {
	$code .=  '
<label for="' . $value . '">' . trans(ucfirst(varname($value, 'blank')), 'html') . '</label>';
	switch ($simpleType[$key]) {
		case 'enum':
			$list = explode(',',$exactType[$key]);
			if (is_array($list)) {
				foreach ($list as $listvalue) {
					$listvalue = trim($listvalue, "'");
					$code .= '
<input type="radio"'.((!$nullable[$key])?' required="required"':'').' name="' . varname($listvalue, 'blank') . '" id="' . varname($listvalue, 'blank') . '[]" value="<?php echo ' . varname($value, $formMethod) . '?>" <?php echo (' . varname($value, $formMethod) . ' == \'' . varname($listvalue, 'blank') . '\''  . ')?\'checked="checked"\':\'\'?> />' . trans(ucfirst(varname($listvalue, 'blank')), 'html') ;
				}
				$code .= '
</select>';
			}
			break;
		case 'set':
		$list = explode(',',$exactType[$key]);
		if (is_array($list)) {
			$code .= '
<select name="' . $value . '" id="' . $value . '" multiple="multiple">';
			foreach ($list as $listvalue) {
				$listvalue = trim($listvalue, "'");
				$code .= '
	<option value="' . varname($listvalue, 'blank'). '" <?php echo (' . varname($value, $formMethod)  . ' == \'' . varname($listvalue, 'blank') . '\'' . (($default[$key] == $listvalue)?' OR !' . varname('submitted', $formMethod) : '') . ')?\'selected\':\'\'?> />' . trans(ucfirst(varname($listvalue, 'blank')), 'html') . '</option>';
			}
			$code .= '
</select>';
		}
			break;
		case 'ckb':
			$code .= '
<input type="checkbox" name="' . $value . '" id="' . $value . '" value="1" <?php echo (' . varname($value, $formMethod)  . (($default[$key] == '1') ? ' OR !' . varname('submitted', $formMethod) : '') . ')?\'checked="checked"\':\'\'?>'.((!$nullable[$key])?' required="required"':'').' />';
			break;
		case 'int':
			$code .= '
<input type="text" name="' . $value . '" id="' . $value . '" value="<?php echo ' . varname($value, $formMethod) . '?>"'.((!$nullable[$key])?' required="required"':'').' />';
			break;
		default:
			if ($shortType[$key] != 'text') {
				$code .= '
<input type="text" name="' . $value . '" id="' . $value . '" value="<?php echo ' . $escapeMethod . '(' . varname($value, $formMethod) . ')?>"'.((!$nullable[$key])?' required="required"':'').' />';
			} else {
				$code .= '
<textarea name="' . $value . '" id="' . $value . '"><?php echo ' . $escapeMethod . '(' . varname($value, $formMethod) . ')?></textarea>';
			}
		}
	$code .=  $sep;
	$code .=  '
<?php if (' . varname($value, 'error') . ') echo ' . varname($value, 'error') . ' . \'' . $sep . '\'?>';
        $code .= '
<br>';
        }
	
}
$code .= '
<input type="hidden" name="submitted" value="submitted">
<input type="submit" id="submit" value="' . trans('Submit', 'html') . '">
</form>';

$formCode = $code;

$endCode = '
</div>
<?php
require("inc/footer.inc.php");';

if ($_REQUEST['form_full']) {
	$htmlFormCode = $initCode . $validateCode . $formSaveCode . $formSelectCode . $formCode . $endCode;
} else {
	$htmlFormCode = $formCode;
}