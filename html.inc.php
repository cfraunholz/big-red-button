<?php
// The Big Red Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8

$first = false;
$code = '';
if ($_REQUEST['custom_html_tags']) {
	$htmlTags = explode(',', $_REQUEST['custom_html_tags']);
	foreach ($htmlTags as $tag) {
		$tag = strip_tags(trim($tag));
		$beforeCode .= '<' . $tag . '>';
	}
	$htmlTagsRevert = array_reverse($htmlTags);
	foreach ($htmlTags as $tag) {
		$tag = strip_tags(trim($tag));
		$attributesStart = strpos($tag, ' ');
		if ($attributesStart) {
			$tag = substr($tag, 0, $attributesStart);
		}
		if (!in_array($tag, array('br','hr', 'input'))) $afterCode .= '</' . $tag . '>';
	}
}
foreach ($array as $key => $value) {
	$code .=  $beforeCode . varname($value, null, 'html') . $afterCode . '
';
}
$htmlCode = stripslashes($code);