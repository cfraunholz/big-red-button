<?php
/**
* The Big Red Red Button Source Code Generator
* Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
* 2010
* v 0.7.8 
**/

$first = false;
$method = (WEBSITE == 'HROSE') ? 'valid':null;
$code = '';
$code .= '
/*** Validation ***/
'.(($_REQUEST['list_paginator'])?'
// index for pagination
validate(\'i\',\'int\');
':'');
foreach ($array as $key => $value) {
	$code .= '
// ' . varname($value, 'blank_ucfirst') . '';
	unset($check);
	switch ($simpleType[$key]) {
		case 'int':
			$phpVal = (($_REQUEST['type_filter']) ? '' : '') . varname($value, $method);
			break;
		case 'ckb':
			$phpVal = varname($value, $method) . ' ? true : false';
			break;
		case 'enum':
			$check = (!$_REQUEST['type_filter']) ? '' : ' AND in_array(' . varname($value, $method) . ', array(' . $exactType[$key] . ')';
			$phpVal = varname($value);
			break;
		case 'set':
			$check = (!$_REQUEST['type_filter']) ? '' : ' AND in_array(' . varname($value, $method) . ', array(' . $exactType[$key] . ')';
			$phpVal = varname($value, $method);
            $set = ', '.str_replace('set','array',$fullType[$key]);
			break;
		default:
			$phpVal = varname($value, $method);
	}
        $code .= '
validate(\'' . varname($value, 'blank') . '\', \''.$simpleType[$key].(($nullable[$key])?' nullable':'').'\''.(($set)?$set:'').');
';
        
	if ($_REQUEST['validate_store_session'] AND ($_REQUEST['type'] == 'list')) {$code .= '$_SESSION[$modul][\'' . $value . '\'] = ' . $phpVal . ';';
	}
	if (!$nullable[$key] AND $simpleType[$key] != 'ckb') {
		if (!$_REQUEST['individual_errors']) {
			$mandatoryField[] = '!' . varname($value);
		} else {
			$code .= ' else {
	' . varname($value, 'error') . ' = ' . trans('%s missing', 'php', varname($value, 'blank_ucfirst'))  . ';
}';
		}
	}
	if ($_REQUEST['validate_store_session'] AND ($_REQUEST['type'] == 'list')) {
		$code .= '
' . varname($value, 'varname') . ' = ' . '$_SESSION[$modul][\'' . $value . '\'];
';
	}
}

if (!$_REQUEST['individual_errors'] AND $mandatoryField AND $_REQUEST['type'] != 'list') {
	$code .= '
if (isset($_REQUEST[\'submitted\'])) {
if (' . implode(' OR ', $mandatoryField) . ') {
	$headerError = ' . trans('Some mandatory fields are missing', 'php') . ';
}';
}
$validateCode = $code;
?>