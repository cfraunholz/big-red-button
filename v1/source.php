<?php
// Red Button v0.9
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
?>
<html>
<head>
<title>PHP10.de The Big Red Button Source Code Generator Source</title>
</head>
<body>
<h1>The Big Red Button Source Code Generator Source Code</h1>
<ul>
<li><a href="src.all.php">red_button.zip</a></li>
<li><a href="src.index.php">index.php</a></li>
<li><a href="src.html.php">html.inc.php</a></li>
<li><a href="src.html_form.php">html_form.inc.php</a></li>
<li><a href="src.html_list.php">html_list.inc.php</a></li>
<li><a href="src.mysql_values.php">mysql_values.inc.php</a></li>
<li><a href="src.validate.php">validate.inc.php</a></li>
<li><a href="License.txt">License.txt</a></li>
</body>
</html>