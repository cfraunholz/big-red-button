<?php
// The Big Red Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8
$first = false;
$escapeMethod = 'htmlspecialchars';
$sep = '<br>';



/***** Select *****/
$code = '';

$pkfirst = false;
$pkCheckCode = '';
$pkSelectCode = '';
foreach ($primary as $key => $pk) {
	if ($pkfirst) {
		$pkCheckCode .= ' AND ';
		$pkSelectCode .= (($pkLast == 'int') ? ' . "' : '') . ' AND ';
	}
	$pkCheckCode .= varname($pk);
	$pkSelectCode .= varname($pk, 'blank') . ' = ';
	if (in_array($simpleType[$key], array('int', 'ckb'))) {
		$pkSelectCode .= '" . (int) ' . varname($pk);
		$pkLast = 'int';
	} else {
		$pkSelectCode .= '\'" . ' . varname($pk) . ' . "\'"';
	}
	$pkfirst = true;
}

$code .= '
if (' . $pkCheckCode . ') {
	$sql = "SELECT * FROM jobs WHERE ' . $pkSelectCode . ';
	$data = mysql_fetch_assoc(mysql_query($sql));
	foreach ($data as $key => $value) {
		$$key = $value;
	}
}
';

$formSelectCode = $code;


/***** Save *****/
$code = '';

$code = '
if (' . varname('submitted') . ' AND !$error) {
	' . str_replace('INSERT', 'REPLACE', $mysqlValuesCode) . ';
	mysql_query($sql) or die(' . trans('error saving content') . ');
}
';

$formSaveCode = $code;


/***** Form *****/
$code = '';
if ($_REQUEST['form_full']) {
	$formMethod = 'varname';
	if (!$_REQUEST['form_field_errors']) {
		$code = '
if ($error) {
	echo \'<p class="error">\' . implode(\'<br>\', $error) . \'</p>\';
}';
	}
}

$code .= '
?>

<form id="form1" name="form1" method="post" action="">';
foreach ($array as $key => $value) {
	$code .=  '
<label for="' . $value . '">' . trans(ucfirst(varname($value, 'blank')), 'html') . '</label>';
	switch ($simpleType[$key]) {
		case 'enum':
			$list = explode(',',$exactType[$key]);
			if (is_array($list)) {
				foreach ($list as $listvalue) {
					$listvalue = trim($listvalue, "'");
					$code .= '
<input type="radio" name="' . varname($listvalue, 'blank') . '" id="' . varname($listvalue, 'blank') . '[]" value="<?php echo ' . varname($value, $formMethod) . '?>" <?php echo (' . varname($value, $formMethod) . ' == \'' . varname($listvalue, 'blank') . '\''  . (($default[$key] == $listvalue) ? ' OR !' . varname('submitted', $formMethod) : '') . ')?\'checked="checked"\':\'\'?> />' . trans(ucfirst(varname($listvalue, 'blank')), 'html') ;
				}
				$code .= '
</select>';
			}
			break;
		case 'set':
		$list = explode(',',$exactType[$key]);
		if (is_array($list)) {
			$code .= '
<select name="' . $value . '" id="' . $value . '">';
			foreach ($list as $listvalue) {
				$listvalue = trim($listvalue, "'");
				$code .= '
	<option value="' . varname($listvalue, 'blank'). '" <?php echo (' . varname($value, $formMethod)  . ' == \'' . varname($listvalue, 'blank') . '\'' . (($default[$key] == $listvalue)?' OR !' . varname('submitted', $formMethod) : '') . ')?\'selected\':\'\'?> />' . trans(ucfirst(varname($listvalue, 'blank')), 'html') . '</option>';
			}
			$code .= '
</select>';
		}
			break;
		case 'ckb':
			$code .= '
<input type="checkbox" name="' . $value . '" id="' . $value . '" value="1" <?php echo (' . varname($value, $formMethod)  . (($default[$key] == '1') ? ' OR !' . varname('submitted', $formMethod) : '') . ')?\'checked="checked"\':\'\'?> />';
			break;
		case 'int':
			$code .= '
<input type="text" name="' . $value . '" id="' . $value . '" value="<?php echo ' . varname($value, $formMethod) . '?>" />';
			break;
		default:
			if ($shortType[$key] != 'text') {
				$code .= '
<input type="text" name="' . $value . '" id="' . $value . '" value="<?php echo ' . $escapeMethod . '(' . varname($value, $formMethod) . (($charset == 'UTF-8') ? ', ENT_QUOTES, \'UTF-8\'' : '') . ')?>" />';
			} else {
				$code .= '
<textarea name="' . $value . '" id="' . $value . '"><?php echo ' . $escapeMethod . '(' . varname($value, $formMethod) . (($charset == 'UTF-8') ? ', ENT_QUOTES, \'UTF-8\'' : '') . ')?></textarea>';
			}
		}
	$code .=  $sep;
	$code .=  '
<?php if (' . varname($value, 'error') . ') echo ' . varname($value, 'error') . ' . \'' . $sep . '\'?>';
	
}
$code .= '
<input type="hidden" name="submitted" value="submitted">
<input type="submit" id="submit" value="' . trans('Submit', 'html') . '">
</form>';

$formCode = $code;

if ($_REQUEST['form_full']) {
	$htmlFormCode = $validateCode . $formSaveCode . $formSelectCode . $formCode;
} else {
	$htmlFormCode = $formCode;
}