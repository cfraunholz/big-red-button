<?php
// The Big Red Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8
$first = false;
$code = '';
$code .= '$sql = "INSERT INTO ' . (($tableName)?$tableName:'tableName') . (($_REQUEST['mysql_full']) ? '(' . $blankList . ')':'') . ' VALUES(';
foreach ($array as $key => $value) {
	if ($first) $code .= ', ' . chr(13);
	if ($simpleType[$key] != 'int') $code .= '\'" . mysql_real_escape_string(';
	else {
		$code .= (!$first) ? '" .':'';
		if ($_REQUEST['mysql_int_typecast']) $code .= ' (int) ';
	}
	$code .= varname($value);
	if ($simpleType[$key] != 'int') $code .= ') . "\'';
	else $code .= (!$first) ? ' . "' : '';
	$first = true;
}
$code .= (!$first) ? '"' : '';
if (strpos($code, ' . ""', strlen($code) - 5) !== false) {
	$code = substr($code, 0, -4);
}
$code .= ')";';
$mysqlValuesCode = $code;