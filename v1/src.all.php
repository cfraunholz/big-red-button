<?php
$fileToZip[]="License.txt";
$fileToZip[]="index.php";
$fileToZip[]="validate.inc.php";
$fileToZip[]="mysql_values.inc.php";
$fileToZip[]="html.inc.php";
$fileToZip[]="html_form.inc.php";
$fileToZip[]="html_list.inc.php";
$fileToZip[]="red_button.png";

$directoryToZip="./"; // This will zip all the file(s) in this present working directory

$outputDir=""; //Replace "/" with the name of the desired output directory.
$zipName="red_button.zip";

include_once("CreateZipFile.inc.php");
$createZipFile=new CreateZipFile;


// Code to Zip a single file
//$createZipFile->addDirectory($outputDir);
foreach ($fileToZip as $file) {
	$fileContents=file_get_contents($file);
	$createZipFile->addFile($fileContents, $outputDir.$file);
}

//Code toZip a directory and all its files/subdirectories
/*
$createZipFile->zipDirectory($directoryToZip,$outputDir);
*/

$rand=md5(microtime().rand(0,999999));
//$zipName=$rand."_".$zipName;
$fd=fopen($zipName, "wb");
$out=fwrite($fd,$createZipFile->getZippedfile());
fclose($fd);
$createZipFile->forceDownload($zipName);
@unlink($zipName);
?>