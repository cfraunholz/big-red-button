<?php
// The Big Red Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8
$first = false;
if (!$listMethod) $listMethod = 'row';
if (!$searchMethod) $searchMethod = '_SESSION';


/***** Init *****/
$code = '';

if ($_REQUEST['list_search']) {
	$code .= '
session_start();';
}

if ($_REQUEST['list_sort']) {
	$code .= '
define(\'DEFAULT_SORT\', \'' . varname($primary[0]) . '\');';
}

if ($_REQUEST['list_paginator']) {
	$code .= '
define(\'ROWS_PER_PAGE\', \'50\');';
}

$code .= $code ? '
' :'';

if ($_REQUEST['list_search']) {
	$code .= '
if (isset(' . varname('sort') . ')) {
	' . varname('sort', $searchMethod) . ' = ' . varname('sort') . ';
}';
	foreach ($array as $key => $value) {
			$code .= '
if (isset(' . varname($value) . ')) {
	' . varname($value, $searchMethod) . ' = ' . varname($value) . ';
};';
	}
}
if ($_REQUEST['list_sort']) {
	$code .= '
if (isset(' . varname('sort') . ')) {
	' . varname('sort', $searchMethod) . ' = ' . varname('sort') . ';
}';
}
if ($_REQUEST['list_paginator']) {
	$code .= '
if (isset(' . varname('page') . ')) {
	' . varname('page', $searchMethod) . ' = ' . varname('page') . ';
}
';
}

$listInitCode = $code;


/***** DELETE statement ****/
$code = '';

$pkfirst = false;
foreach ($primary as $key => $pk) {
	if ($pkfirst) $pkDelCode .= (($pkLast == 'int') ? ' . "' : '') . ' AND ';
	$pkDelCode .= varname($pk, 'blank') . ' = ';
	if (in_array($simpleType[$key], array('int', 'ckb'))) {
		$pkDelCode .= '" . (int) ' . varname($pk);
		$pkLast = 'int';
	} else {
		$pkDelCode .= '\'" . ' . varname($pk) . ' . "\'"';
	}
	$pkfirst = true;
}
$code .= '
// delete
if (isset(' . varname('delete') . ')) {
	$sql = "DELETE FROM ' . $tableName . ' WHERE ' . $pkDelCode . ';
	mysql_query($sql) or error_log(mysql_error());
}
';

$listDeleteCode = $code;


/***** Search ****/
$code = '';


if ($_REQUEST['list_sort']) {
	$code .= '
// Sort
if (' . varname('sort', $searchMethod) . ') {
	$sortAr = explode(\' \', ' . varname('sort', $searchMethod) . ');
	$order_name = $sortAr[0];
	$order_dir = $sortAr[1];
	if (in_array($order_name, array(\'' . implode('\', \'', $array) . '\'))) {
		$orderby = ((strtoupper($order_dir) == \'DESC\') ? \'DESC\' : \'ASC\';
	} else {
		$orderby = DEFAULT_SORT;
	}
}
';
}

if ($_REQUEST['list_paginator']) {
	$code .= '
// Pagination
$offset = (' . varname('page', $searchMethod)  . '-1) * ROWS_PER_PAGE;
';
}

$code .= '
// where condition';
foreach ($array as $key => $value) {
	$code .= '
if (' . varname($value, $searchMethod) . ') {';
	switch ($simpleType[$key]) {
		case 'ckb':
			$code .= '
	$where[] = "' . varname($value, 'blank') . ' = " . ((' . varname($value, $searchMethod) . ' == \'y\') ? 1 : 0);';
			break;
		case 'string':
			$code .= '
	$where[] = "' . varname($value, 'blank') . ' LIKE \'%" . mysql_real_escape_string(' . varname($value, $searchMethod) . ') . "%\'";';
			break;
		case 'int':
			$code .= '
	$where[] = "' . varname($value, 'blank') . ' =  " . (int) ' . varname($value, $searchMethod) . ';';
			break;
		default:
			$code .= '
	$where[] = "' . varname($value, 'blank') . ' =  \'" . mysql_real_escape_string(' . varname($value, $searchMethod) . ') . "\'";';
	}
	$code .= '
}';
}
$code .= '
$where = ($where) ? implode(" AND ", $where) : "1=1";';

$listSearchCode = $code;


/***** SELECT ****/
$code = '';
$code .= '
$sql = "SELECT ' . (($_REQUEST['list_paginator']) ? 'SQL_CALC_FOUND_ROWS ' : '') .'* FROM ' . $tableName . ' WHERE ' . (($_REQUEST['list_search']) ? '" . $where' : '1=1') . (($_REQUEST['list_sort']) ? ' ORDER BY " . $orderby' : '') . (($_REQUEST['list_paginator']) ? ' . " LIMIT " . $offset . ", " . ROWS_PER_PAGE' : '') . ';
$result = mysql_query($sql);
';

$listSelectCode = $code;


/***** The table ****/
$code = '';
$code = '
<table>
<tr>';

if ($_REQUEST['dynamic_list_header']) {
	$code .= '
<?php
$header = array(\'' . implode(',', $array) . '\');
foreach ($header as $key => $value) {';
	if ($_REQUEST['list_sort']) {
		$code .= '
	if ($order_name == $value) {
		if ($order_dir == \'DESC\') {
			$order_dir = \'ASC\';
			$order_class = \'DESC\';
		} else {
			$order_dir = \'DESC\';
			$order_class = \'ASC\';
		}
	} else {
		$order_class = \'\';
		$order_dir = \'ASC\';
	}	
	echo \'<th\' . (($order_class) ? \' class= "\' . $order_class . \'"\' : \'\') . \'><a href="?sort=\' . urlencode($value . \' \' . $order_dir) . \'">\' . ucfirst($value) . \'</a></th>\';';
	} else {
		$code .= '
	echo \'<th>\' . ucfirst($value) . \'</th>\';
	';
	}
	$code .= '
}
?>';
} else {
	foreach ($array as $key => $value) {
		$code .=  '
<th>' . ucfirst(varname($value, 'blank')) . '</th>';
	}
}

$code .= '
</tr>
<?php
if (!mysql_num_rows($result)) {
	echo \'<tr><td colspan=' . count($array) . '>' . trans('no entries') . '</td></tr>\';
} else {
	while($row = mysql_fetch_array($result)) {
		echo \'<tr class="\' .  ((($i++ % 2)==0) ? "tr_even":"tr_odd") . \'">\';';
foreach ($array as $key => $value) {
	switch ($simpleType[$key]) {
		case 'ckb':
			$code .=  '
			echo \'<td>\' . ((' . varname($value, $listMethod) . ' == \'1\') ? ' . trans('yes', 'php') . ' : ' . trans('no', 'php') . ') . \'</td>\';';
			break;
		case 'enum':
			$code .=  '
			echo \'<td>\' . ' . ucfirst(varname($value, $listMethod)) . ' . \'</td>\';';
			break;
		case 'set':
			$code .=  '
			echo \'<td>\' . ' . ucfirst(varname($value, $listMethod)) . ' . \'</td>\';';
			break;
		case 'integer':
			$code .=  '
			echo \'<td style="text-align:\'right\'">\' . ' . ucfirst(varname($value, $listMethod)) . ' . \'</td>\';';
			break;
		default:
		$code .=  '
			echo \'<td>\' . ' . varname($value, $listMethod) . ' . \'</td>\';';
	}
}

$pkfirst = false;
$pkUrl = '';
foreach ($primary as $key => $pk) {
	if ($pkfirst) $pkUrl .= '&amp;';
	switch ($simpleType[$key]) {
		case 'int':
			$escaped = '(int) ' . varname($pk, $listMethod);
			break;
		case 'ckb':
			$escaped = '(int) ' . varname($pk, $listMethod);
			break;
		default:
			$escaped = 'htmlspecialchars(' . varname($pk, $listMethod) . (($charset == 'UTF-8') ? ', ENT_QUOTES, \'UTF-8\'' : '') . ')';
	}
	$pkUrl .= $pk . '=\' . ' . $escaped . ' . \'';
	$pkfirst = true;
}
$code .=  '
		echo \'<td><a href="detail.php?' . $pkUrl . '">' . trans('Update') . '</a></td>\';
		echo \'<td><a href="?delete=1&amp;' . $pkUrl . '">' . trans('Delete') . '</a></td>\';
		echo \'</tr>\';
	}
}';

$code .= '
?>
</table>
';
$tableCode = $code;


/***** Pagination ****/
$code = '';

$code .= '
<?php
// Pagination
$sql = "SELECT FOUND_ROWS() as rownum";
$row = mysql_fetch_row(mysql_query($sql));
$totalRows = $row[0];

$pageNum = ceil($totalRows / ROWS_PER_PAGE);
$paginator = "";
for ($i = 1; $i <= $pageNum; $i++) {
	$paginator .= \'<a href="?page=\' . $i . \'">\' . (($i == ' . varname('page', $searchMethod) . ') ? \'<b>[\' . $i . \']</b>\' : $i) . \'</a>&nbsp;\';
}
echo $paginator;
';

$paginationCode = $code;


if ($_REQUEST['list_search']) {
	$htmlListCode = $listInitCode . $listDeleteCode . $listSearchCode . $listSelectCode . $formCode . $tableCode . (($_REQUEST['list_search']) ? $paginationCode : '');
} else {
	$htmlListCode = $listInitCode . $listDeleteCode . $listSearchCode . $tableCode . (($_REQUEST['list_search']) ? $paginationCode : '');
}