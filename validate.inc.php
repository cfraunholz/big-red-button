<?php
/**
* The Big Red Red Button Source Code Generator
* Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
* 2010
* v 0.7.8 
**/

$first = false;
$code = '';
$code .= '
// Validation
';
foreach ($array as $key => $value) {
	$code .= '
// ' . varname($value, 'blank_ucfirst') . '';
	unset($check);
	switch ($simpleType[$key]) {
		case 'int':
			$phpVal = (($_REQUEST['type_filter']) ? '(int) ' : '') . varname($value);
			break;
		case 'ckb':
			$phpVal = varname($value) . ' ? true : false';
			break;
		case 'enum':
			$check = (!$_REQUEST['type_filter']) ? '' : ' AND in_array(' . varname($value) . ', array(' . $exactType[$key] . ')';
			$phpVal = varname($value);
			break;
		case 'set':
			$check = (!$_REQUEST['type_filter']) ? '' : ' AND in_array(' . varname($value) . ', array(' . $exactType[$key] . ')';
			$phpVal = varname($value);
			break;
		default:
			$phpVal = varname($value);
	}
	if ($_REQUEST['validate_store_session']) {
		$code .= '
if (isset(' . varname($value) . ') AND ' . varname($value) . $check . ') ) {
	$_SESSION[\'' . $value . '\'] = ' . $phpVal . ';
}';
	} else {
		$code .= '
if (' . varname($value) . '' . (($simpleType[$key] == 'enum' AND $_REQUEST['type_filter']) ? ' AND in_array(' . varname($value) . ', array(' . $exactType[$key] . '))' : '') . ') {
	' . varname($value, 'varname') . ' = '. $phpVal . ';
}';
	}
	if (!$nullable[$key] AND $simpleType[$key] != 'ckb') {
		if (!$_REQUEST['individual_errors']) {
			$mandatoryField[] = '!' . varname($value);
		} else {
			$code .= ' else {
	' . varname($value, 'error') . ' = ' . trans('%s missing', 'php', varname($value, 'blank_ucfirst'))  . ';
}';
		}
	}
	if ($_REQUEST['validate_store_session']) {
		$code .= '
' . varname($value, 'varname') . ' = ' . '$_SESSION[\'' . $value . '\'];
';
	}
}

if (!$_REQUEST['individual_errors'] AND $mandatoryField) {
	$code .= '
if (' . implode(' OR ', $mandatoryField) . ') {
	$error[] = ' . trans('missing fields', 'php') . ';
}';
}
$validateCode = $code;
?>