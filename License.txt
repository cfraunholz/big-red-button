The Big Red Button Source Code Generator is published unter the Mozilla Public License Version 1.1 (MPL). This license guarantees you the free usage of The Big Red Button Source Code Generator, access to the sourcecode and the right to modify and distribute The Big Red Button Source Code Generator.

The only restrictions apply to the copyright, which remains at all times at Christian Fraunholz. Any modified versions of The Big Red Button Source Code Generator will also fall under the terms of MPL. Any other program, that may only be accessing certain functions of The Big Red Button Source Code Generator is not affected by these restrictions and may be distributed under any type of license.

A commercial usage or commercially distribution of The Big Red Button Source Code Generator, e.g. on CD-ROMs, is allowed, as long as the conditions mentioned above are met.

We decided to use MPL as the licensing model for The Big Red Button Source Code Generator because we feel that it is a good compromise between the protection of the openness and free distribution on the one hand and the interaction with other software regardless of its licensing model. When compared to other licensing models its text is short and easily comprehensible.

