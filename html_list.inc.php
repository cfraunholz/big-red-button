<?php
// The Big Red Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8
$first = false;
if (!$listMethod) $listMethod = 'row';
if (WEBSITE == 'HROSE') {
    if (!$searchMethod) $searchMethod = '_VALID';
    if (!$searchDBMethod) $searchDBMethod = '_VALIDDB';
} else {
    if (!$searchMethod) $searchMethod = '_SESSION';
    if (!$searchDBMethod) $searchDBMethod = '_SESSION';
}


/***** Init *****/
$code = '$modul="'.$tableName.'";

require("inc/req.php");

/*** Rights ***/
// Generally for people with the right to change ' . $tableName . '
GRGR(1000);
';

/*
if ($_REQUEST['list_search']) {
	$code .= '
session_start();';
}

if ($_REQUEST['list_sort']) {
	$code .= '
define(\'DEFAULT_SORT\', \'' . varname($primary[0]) . '\');';
}

if ($_REQUEST['list_paginator']) {
	$code .= '
define(\'ROWS_PER_PAGE\', \'50\');';
}
*/
$code .= $code ? '
' :'';

if ($_REQUEST['list_search']) {

if ($_REQUEST['list_ajax_??']) {	$code .= '
if (isset(' . varname('sort') . ')) {
	' . varname('sort', $searchMethod) . ' = ' . varname('sort') . ';
}';
	foreach ($array as $key => $value) {
			$code .= '
if (isset(' . varname($value) . ')) {
	' . varname($value, $searchMethod) . ' = ' . varname($value) . ';
};';
   }
}
if ($_REQUEST['list_sort_alt']) {
	$code .= '
if (isset(' . varname('sort') . ')) {
	' . varname('sort', $searchMethod) . ' = ' . varname('sort') . ';
}';
}
if ($_REQUEST['list_paginator']) {
	$code .= '
if (isset(' . varname('page') . ')) {
	' . varname('page', $searchMethod) . ' = ' . varname('page') . ';
}
';
        
} else {
    $code .= '
/*** General Table variables **/
// fill parameters from session
if (!isset($_REQUEST[\'headless\']) && isset($_SESSION[$modul])) {
    foreach ($_SESSION[$modul] as $key => $value) {
        $_REQUEST[$key] = $value;
    }
}';
if ($_REQUEST['list_sort']) {
  if (in_array('name', $array)) {
    $sort = 'name';
  } else if (in_array('shortname',$array)) {
    $sort = 'shortname';
  } else {
    $sort = $array[0];
  }
	$code .= '
if (isset($_REQUEST[\'sortcol\'])) {
   $_SESSION[$modul][\'sortcol\'] = $_REQUEST[\'sortcol\'];
}
if (isset($_REQUEST[\'sortdir\'])) {
   $_SESSION[$modul][\'sortdir\'] = $_REQUEST[\'sortdir\'];
}
validate(\'sortcol\', \'string\');
validate(\'sortdir\', \'set\', array(\'ASC\',\'DESC\'));
$orderBy = ($_VALID[\'sortcol\'])?$_VALID[\'sortcol\'] .(($_VALID[\'sortdir\'])?\' \' . $_VALID[\'sortdir\']:\'\'):\''.$sort.'\';';
}
$code .= '
    
$headless = (isset($_REQUEST[\'headless\']))?true:false;
$n4a[\''.$tableName.'_d.php\'] = \''.trans('Add '.ucfirst($tableName)).'\';
if (!$headless) require("inc/header.inc.php");
';
}
}

$listInitCode = $code;


/***** DELETE statement ****/
$code = '';

$pkfirst = false;
foreach ($primary as $key => $pk) {
	if ($pkfirst) $pkDelCode .= (($pkLast == 'int') ? ' . "' : '') . ' AND ';
	$pkDelCode .= varname($pk, 'blank') . ' = ';
	if (in_array($simpleType[$key], array('int', 'ckb'))) {
		$pkDelCode .= '" . (int) ' . varname($pk);
		$pkLast = 'int';
	} else {
		$pkDelCode .= '\'" . ' . varname($pk) . ' . "\'"';
	}
	$pkfirst = true;
}
$code .= '
// delete
if (isset(' . varname('delete','_REQUEST') . ')) {
	$sql = "DELETE FROM ' . $tableName . ' WHERE ' . $pkDelCode . ';
	mysql_query($sql) or error_log(mysql_error());
}
';

$listDeleteCode = $code;


/***** Search ****/
$code = '';


if ($_REQUEST['list_sort_alt']) {
	$code .= '
// Sort
if (' . varname('sort', $searchMethod) . ') {
	$sortAr = explode(\' \', ' . varname('sort', $searchMethod) . ');
	$order_name = $sortAr[0];
	$order_dir = $sortAr[1];
	if (in_array($order_name, array(\'' . implode('\', \'', $array) . '\'))) {
		$orderBy = ((strtoupper($order_dir) == \'DESC\') ? \'DESC\' : \'ASC\';
	} else {
		$orderBy = DEFAULT_SORT;
	}
}
';
}

if ($_REQUEST['list_paginator']) {
	$code .= '
// Pagination
$offset = (' . varname('page', $searchMethod)  . '-1) * ROWS_PER_PAGE;
';
}

$code .= '
// where condition';
foreach ($array as $key => $value) {
  if ($simpleType[$key] == 'ckb') {
	$code .= '
if (isset(' . varname($value, $searchMethod) . ')) {';
  } else {
	$code .= '
if (' . varname($value, $searchMethod) . ') {';
  }
	switch ($simpleType[$key]) {
		case 'ckb':
			$code .= '
	$where[] = "' .$tableName . '.' . varname($value, 'blank') . ' = " . ((' . varname($value, $searchDBMethod) . ' == 1) ? 1 : 0);';
			break;
		case 'string':
			$code .= '
	$where[] = "' .$tableName . '.'. varname($value, 'blank') . ' LIKE \'%" . mysql_real_escape_string(' . varname($value, $searchMethod) . ') . "%\'";';
			break;
		case 'int':
			$code .= '
	$where[] = "' .$tableName . '.'. varname($value, 'blank') . ' =  " . '.((WEBSITE == HROSE)?'':'(int) ') . varname($value, $searchDBMethod) . ';';
			break;
		default:
			$code .= '
	$where[] = "' .$tableName . '.'. varname($value, 'blank') . ' =  \'" . mysql_real_escape_string(' . varname($value, $searchMethod) . ') . "\'";';
	}
	$code .= '
}
$_SESSION[$modul][\''.$value.'\'] = $_VALID[\''.$value.'\'];';
}
$code .= '
$where = ($where) ? implode(" AND ", $where) : "1=1";';

$listSearchCode = $code;


/***** SELECT ****/
$code = '';
$code .= '
$sql = "SELECT ' . (($_REQUEST['list_paginator']) ? 'SQL_CALC_FOUND_ROWS ' : '') .$tableName . '.' . implode(', '.$tableName.'.',$array).' FROM ' . $tableName . ' WHERE ' . (($_REQUEST['list_search']) ? '" . $where . "' : '1=1') . (($_REQUEST['list_sort']) ? ' ORDER BY " . $orderBy' : '') . (($_REQUEST['list_paginator']) ? ' . " LIMIT " . $offset . ", " . ROWS_PER_PAGE' : '') . ';';
if ($_REQUEST['list_memcache']) {
    $code .= '
/*** Store SQL in Session ***/
$_SESSION[$modul][\'sql\'] = $sql;
$listResult = getMemCache($sql);
// refresh memcache if necessary
$rl = isset($_SESSION[$modul][\'rl\']) || isset($_GET[\'rl\']);
if (!$listResult || $rl) {
    $r = mysql_query($sql) or die(mysql_error());
    unset($listResult);
    while($row=mysql_fetch_array($r))
        $listResult[]=$row;
    if ($memcache) {
        setMemCache($sql, $listResult);
    }
    unset($_SESSION[$modul][\'rl\']);
}';
} else {
    $code .= '
$listResult = mysql_query($sql);
';
}

$listSelectCode = $code;


/***** The table ****/
$code = '';
if ($_REQUEST['list_ajax']) {
    $code = '
if (!$headless) {
?>
<div class="contentheadline"><?php echo '.trans(ucfirst($tableName),'php') . '?></div>
<br>
<div class="contenttext">
<table cellspacing="0" cellpadding="0" class="bw">
<?php
}
if (!$headless) {
    echo \'<tr class="head">\';';
    $i = 0;
    foreach ($array as $field) {
        $code .= '
    echo \'<th'.(($_REQUEST['list_odd_column'] AND (($i++ % 2)==0))?'':' class="grey"').'><a href="javascript:void(0)" onClick="changeSort(\\\''.$field.'\\\')">'.trans($field).'</a>&nbsp;&nbsp;
           <input class="search" name="'.$field.'" id="'.$field.'" value="\'.$_SESSION[$modul][\''.ucfirst($field).'\'].\'">
           </th>\';';
        }
    $code .= '
    echo \'<th>&nbsp;</th>\';
    echo \'</tr><tbody id="list_tbody">\';
}';
} else {
$code = '
<table>
<tr>';

if ($_REQUEST['dynamic_list_header']) {
	$code .= '
<?php
$header = array(\'' . implode(',', $array) . '\');
foreach ($header as $key => $value) {';
	if ($_REQUEST['list_sort'] && !$_REQUEST['list_ajax']) {
		$code .= '
	if ($order_name == $value) {
		if ($order_dir == \'DESC\') {
			$order_dir = \'ASC\';
			$order_class = \'DESC\';
		} else {
			$order_dir = \'DESC\';
			$order_class = \'ASC\';
		}
	} else {
		$order_class = \'\';
		$order_dir = \'ASC\';
	}	
	echo \'<th\' . (($order_class) ? \' class= "\' . $order_class . \'"\' : \'\') . \'><a href="?sort=\' . urlencode($value . \' \' . $order_dir) . \'">\' . ucfirst($value) . \'</a></th>\';';
	} else if(!$_REQUEST['list_sort']) {
		$code .= '
	echo \'<th>\' . ucfirst($value) . \'</th>\';
	';
	} else {
		$code .= '
	echo \'<th href="javascript:void(0)" onClick="changeSort(\'.$value.\')">\' . ucfirst($value) . \'</th>\';
	';
	}
	$code .= '
}
?>';
} else {
	foreach ($array as $key => $value) {
		$code .=  '
<th>' . ucfirst(varname($value, 'blank')) . '</th>';
	}
}
}
if (!$_REQUEST['list_ajax']) $code .= '
</tr>
<?php';
$code .= '
  if (!$listResult) {
    echo \'<tr><td colspan="3">\'.' . trans('No entries found', 'php') . '.\'</td></tr>\';
  } else {
    $i = 0;
	'.(($_REQUEST['list_memcache'])?'foreach($listResult as $index => $row) {':'while($row = mysql_fetch_array($listResult)) {').'
		echo \'<tr'.(($_REQUEST['list_odd_row'])?' class="dotted \' .  ((($i++ % 2)==0) ? "tr_even":"tr_odd") . \'"':'').' id="tr_\'.$row[\''.$tableName.'_id\'].\'">\';';

$i = 0;
foreach ($array as $key => $value) {
    if ($i++ == 0) $mouseover = ((WEBSITE == HROSE)?' \'.$mouseover.\'':'').' onClick="location.href=\\\''.$tableName.'_d.php?i=\'.$index.\'&amp;'.$tableName.'_id=\'.$row[\''.$tableName.'_id\'].\'\\\'"';
    else $mouseover = '';
	switch ($simpleType[$key]) {
		case 'ckb':
			$code .=  '
			echo \'<td'.$mouseover.' nowrap>\' . ((' . varname($value, $listMethod) . ' == \'1\') ? ' . trans('yes', 'php') . ' : ' . trans('no', 'php') . ') . \'</td>\';';
			break;
		case 'enum':
			$code .=  '
			echo \'<td'.$mouseover.' nowrap>\' . ' . ucfirst(varname($value, $listMethod)) . ' . \'</td>\';';
			break;
		case 'set':
			$code .=  '
			echo \'<td'.$mouseover.' nowrap>\' . ' . ucfirst(varname($value, $listMethod)) . ' . \'</td>\';';
			break;
		case 'integer':
			$code .=  '
			echo \'<td'.$mouseover.' style="text-align:\'right\'" nowrap>\' . ' . ucfirst(varname($value, $listMethod)) . ' . \'</td>\';';
			break;
		default:
		$code .=  '
			echo \'<td'.$mouseover.' nowrap>\' . ' . varname($value, $listMethod) . ' . \'</td>\';';
	}
}

$pkfirst = false;
$pkUrl = '';
foreach ($primary as $key => $pk) {
	if ($pkfirst) $pkUrl .= '&amp;';
	switch ($simpleType[$key]) {
		case 'int':
			$escaped = '(int) ' . varname($pk, $listMethod);
			break;
		case 'ckb':
			$escaped = '(int) ' . varname($pk, $listMethod);
			break;
		default:
			$escaped = 'htmlspecialchars(' . varname($pk, $listMethod) . (($charset == 'UTF-8') ? ', ENT_QUOTES, \'UTF-8\'' : '') . ')';
	}
	$pkUrl .= $pk . '=\' . ' . $escaped . ' . \'';
	$pkfirst = true;
}
$code .=  '
            echo \'<td nowrap><a href="'.$tableName.'_d.php?i='.$index.'&amp;' . $pkUrl . '">' . ((WEBSITE == HROSE)?'<img src="css/icon/pencil_icon&16.png" title="'.trans('Edit').'">':trans('Update')).'</a>\';';
if ($_REQUEST['list_ajax']) 
$code .= "
// people with right to delete see the delete button
    if (R(3))
            echo '&nbsp;&nbsp;<a href=\"#\" onclick=\"if (confirm(\'" . trans('Do you really want to delete the '.ucfirst($tableName).'?') . "\')) delRow('.\$row['".$tableName."_id'].');\">
            <img src=\"css/icon/delete_icon&16.png\" title=\"" . trans('Delete') . "\"></a>';
            echo '</td>';";
else 
    $code .= '
            echo \'&nbsp;&nbsp;<a href="?delete=1&amp;' . $pkUrl . '">' . trans('Delete') . '</a></td>\';';
$code .= '
		echo \'</tr>\';
	}
}';

$code .= '

'.(($_REQUEST['list_ajax'])?'if (!$headless) { ?>':'?>').'
</table>
</div>
';
$tableCode = $code;


/***** Pagination ****/
$code = '';

$code .= '
<?php
// Pagination
$sql = "SELECT FOUND_ROWS() as rownum";
$row = mysql_fetch_row(mysql_query($sql));
$totalRows = $row[0];

$pageNum = ceil($totalRows / ROWS_PER_PAGE);
$paginator = "";
for ($i = 1; $i <= $pageNum; $i++) {
	$paginator .= \'<a href="?page=\' . $i . \'">\' . (($i == ' . varname('page', $searchMethod) . ') ? \'<b>[\' . $i . \']</b>\' : $i) . \'</a>&nbsp;\';
}
echo $paginator;
';

$paginationCode = $code;


/***** jQuery ****/
$code = '';

$code .= "
<script type=\"text/javascript\">";
if ($_REQUEST['list_delete']) {
$code .= "
function delRow(pk) {
    $.ajax({
      url: 'a/".$tableName."_del.php?id='+pk
    });
    $('#tr_'+pk).hide();
}
";
}

if ($_REQUEST['list_search'] || $_REQUEST['list_sort']) {
$code .= "
var sortcol = '".$sort."';
var sortdir = '';
var del = '';
function updateList() {
    var url = '<?=\$_REQUEST['PHP_SELF']?>?headless&sortcol='+sortcol+'&sortdir='+sortdir+'&del='+del;
    var filterparams = '';

".(($_REQUEST['list_search'])?"
    // inputs
    var val = '';
    $('.search:input').each(function(index, obj) {
        val = $('#' + obj.name).val();
        if (val != '') filterparams += '&' + obj.name + '=' + $('#' + obj.name).val();
    });

    $('.bw select').each(function(index, obj) {
        val = $(\"#\" + obj.name).val();
        if (val != '') filterparams += '&' + obj.name + '=' + $('#' + obj.name).val();
    });":"")."

    url += filterparams;
    $.get(url, function(data) {
        $('#list_tbody').html(data);

        // also add the filterparam to the xls export
        $('#xlsbutton').attr('href',$('#xlsbutton').attr('href') + filterparams);
    });

}";
if ($_REQUEST['list_sort'])
$code .= "
function changeSort(col) {

    if (sortcol == col) {
        sortdir = (sortdir == 'DESC') ? 'ASC' : 'DESC';
    } else {
        sortdir = 'DESC';
    }
    sortcol = col;

    updateList();
}";
if ($_REQUEST['list_search'])
$code .= "
$('.search:input').keyup(function(index) {
    updateList();
});
$('.bw select').change(function() {
    updateList();
});";
}
$code .= "
</script>
";

$jQueryCode = $code;

/***** Pagination ****/
$code = '';

$code .= '<?php
require("inc/footer.inc.php");
'.(($_REQUEST['list_ajax'])?'}':'');
$footerCode = $code;


if ($_REQUEST['list_search']) {
	$htmlListCode =
        $listInitCode
        . $validateCode
        . $listDeleteCode
        . $listSearchCode
        . $listSelectCode
        . (($_REQUEST['list_ajax']) ? '' : $formCode)
        . $tableCode
        . (($_REQUEST['list_paginator']) ? $paginationCode : '')
        . (($_REQUEST['list_ajax']) ? $jQueryCode : '')
        . $footerCode;
} else {
	$htmlListCode =
        $listInitCode
        . $validateCode
        . $listDeleteCode
        . $listSearchCode
        . $tableCode
        . (($_REQUEST['list_paginator']) ? $paginationCode : '')
        . (($_REQUEST['list_ajax']) ? $jQueryCode : '')
        . $footerCode;
}