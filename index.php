<?php
// The Big Red Button Source Code Generator
// Dipl.-Ing. (FH) Christian K. Fraunholz (php10.de)
// 2010
// v 0.7.8
$modul="red_button";

require '../config.inc.php'; // connect to database
define('TMP_TABLE_NAME', 'red_button_tmp_');
$charset = ($_REQUEST['charset'] == 'ISO') ? 'ISO-8859-1' : 'UTF-8';
header('Content-type: text/html; charset=' . $charset); 
?>
<html>
<head>
<title>PHP10.de The Big Red Button Source Code Generator</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$charset?>">
<style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
h2, h3 {
  margin-bottom: 2px;
}

-->
</style>
</head>
<body style="font-family:Arial,Helvetica">
<div style="width:100%;text-align:right">
<a href="features.php"><img src="Information-icon.png" width="30" alt="Info" title="Info"></a></div>
<script language="JavaScript" type="text/javascript">
<!--
function toggleDiv(element){
      if(document.getElementById(element).style.display = 'none')
      {
        document.getElementById(element).style.display = 'block';
      }
      else if(document.getElementById(element).style.display = 'block')
      {
        document.getElementById(element).style.display = 'none';
      }
	  if(element != 'standard_options') {
        document.getElementById('simple_options').style.display = 'none';
	  }
	  if(element != 'mysql_options') {
        document.getElementById('mysql_options').style.display = 'none';
	  }
	  if(element != 'validate_options') {
        document.getElementById('validate_options').style.display = 'none';
	  }
	  if(element != 'html_options') {
        document.getElementById('html_options').style.display = 'none';
	  }
	  if(element != 'form_options') {
        document.getElementById('form_options').style.display = 'none';
	  }
	  if(element != 'list_options') {
        document.getElementById('list_options').style.display = 'none';
	  }
}
//-->
</script>
<?php
if ($_REQUEST['array']) {
	$_SESSION['array'] = $_REQUEST['array'];
}
if ($_REQUEST['form_errors']) {
	$_REQUEST['individual_errors'] = 'on';
}
$array = $_SESSION['array'];

if(WEBSITE == 'HROSE') {
    if (!$_REQUEST['array']) {
        $array = file_get_contents('array.txt');
    } else {
        file_put_contents('array.txt', $_REQUEST['array']);
        $array = $_REQUEST['array'];
    }
}

if ($_REQUEST['submitted'] AND !$_REQUEST['array']) {
	echo 'Enter a comma separated list or a CREATE TABLE statement.<br>';
}
//if (!$_REQUEST['submitted'] OR !$_REQUEST['array']) :
?>
<form method="post" name="red_button_form">
<textarea name="array" cols="100" rows="5"><? echo ($array)?htmlspecialchars(stripslashes($array), ENT_QUOTES, $charset):'CREATE TABLE example (id INT NOT NULL, name VARCHAR(255), gender enum(\'m\',\'w\') NOT NULL default \'m\', PRIMARY KEY (id))'?></textarea>

<h2>Use function</h2>
<input type="checkbox" name="trim"  <?=($_REQUEST['trim'])?' checked':''?> /> trim<br />
<input type="checkbox" name="custom_method" <?=($_REQUEST['custom_method'] OR $_REQUEST['custom_method_name'])?' checked':''?>/> custom method <input type="text" name="custom_method_name" style="width:270px" value="<?=htmlspecialchars($_REQUEST['custom_method_name'])?>" /><br />
<input type="checkbox" name="arbitrary_before" <?=($_REQUEST['arbitrary_before'] OR $_REQUEST['arbitrary_before_value'])?' checked':''?>/> arbitrary before <input type="text" name="arbitrary_before_value" style="width:270px" value="<?=htmlspecialchars(stripslashes($_REQUEST['arbitrary_before_value']))?>" /><br />
<input type="checkbox" name="arbitrary_after" <?=($_REQUEST['arbitrary_after'] OR $_REQUEST['arbitrary_after_value'])?' checked':''?>/> arbitrary after <input type="text" name="arbitrary_after_value" style="width:270px" value="<?=htmlspecialchars(stripslashes($_REQUEST['arbitrary_after_value']))?>" />

<h2>Target variables </h2>
<input type="radio" name="http_method" value="blank" <?=($_REQUEST['http_method'] == 'blank' OR !$_REQUEST['http_method'])?' checked':''?> /> {foo}
<input type="radio" name="http_method" value="blank_ucfirst" <?=($_REQUEST['http_method'] == 'blank_ucfirst' OR !$_REQUEST['http_method'])?' checked':''?> /> {Foo}
<input type="radio" name="http_method" value="varname" <?=($_REQUEST['http_method'] == 'varname')?' checked':''?>  /> ${foo}
<input type="radio" name="http_method" value="_GET" <?=($_REQUEST['http_method'] == '_GET' OR !$_REQUEST['http_method'])?' checked':''?> /> $_GET['foo']
<input type="radio" name="http_method" value="_POST" <?=($_REQUEST['http_method'] == '_POST')?' checked':''?> /> $_POST['foo']
<input type="radio" name="http_method" value="_SESSION" <?=($_REQUEST['http_method'] == '_SESSION')?' checked':''?> /> $_SESSION['foo']
<input type="radio" name="http_method" value="_REQUEST" <?=($_REQUEST['http_method'] == '_REQUEST')?' checked':''?> /> $_REQUEST['foo']
<input type="radio" name="http_method" value="custom" <?=($_REQUEST['http_method'] == 'custom')?' checked':''?> /> <input type="text" name="custom_http_method" style="width:100px" value="<?=htmlspecialchars(stripslashes($_REQUEST['custom_http_method']))?>" /> $custom['foo']

<h2>Generated Code Type</h2>
<input type="radio" name="type" onClick="toggleDiv('simple_options')" value="simple" <?=($_REQUEST['type'] ==  "simple" OR !$_REQUEST['type'] )?' checked':''?>/> comma-separated list 
<input type="radio" name="type" onClick="toggleDiv('mysql_options')" value="mysql_values" <?=($_REQUEST['type'] == 'mysql_values')?' checked':''?>/> MySQL values
<input type="radio" name="type" onClick="toggleDiv('validate_options')" value="validate" <?=($_REQUEST['type'] == 'validate')?' checked':''?>/> Validation
<input type="radio" name="type" onClick="toggleDiv('html_options')" value="html" <?=($_REQUEST['type'] == 'html')?' checked':''?>/> HTML
<input type="radio" name="type" onClick="toggleDiv('form_options')" value="form" <?=($_REQUEST['type'] == 'form')?' checked':''?>/> Form
<input type="radio" name="type" onClick="toggleDiv('list_options')" value="list" <?=($_REQUEST['type'] == 'list')?' checked':''?>/> List<!--
<input type="radio" name="type" onClick="toggleDiv('list_options')" value="list2" <?=($_REQUEST['type'] == 'list2')?' checked':''?>/> List 2-->

<h2>Options</h2>
<input type="radio" name="charset" value="UTF8" <?=($_REQUEST['charset'] == 'UTF8' OR !$_REQUEST['charset'])?' checked':''?> /> UTF-8
<input type="radio" name="charset" value="ISO" <?=($_REQUEST['charset'] == 'ISO')?' checked':''?> /> ISO
<input type="checkbox" name="trans"  <?=($_REQUEST['trans'] OR !$_REQUEST['type'])?' checked':''?> /> Multilingual

<div id="simple_options" style="display:<?=($_REQUEST['type'] == 'simple' OR !$_REQUEST['type'])?'block':'none'?>">
</div>

<div id="mysql_options" style="display:<?=($_REQUEST['type'] == 'mysql_values')?'block':'none'?>">
<input type="checkbox" name="mysql_int_typecast"  <?=($_REQUEST['mysql_int_typecast'] OR $_REQUEST['type'] != 'mysql_values')?' checked':''?> /> Typecast to Integer
<input type="checkbox" name="mysql_full"  <?=($_REQUEST['mysql_full'] OR $_REQUEST['type'] != 'mysql_values')?' checked':''?> /> 
Full INSERT's<br />
</div>

<div id="validate_options" style="display:<?=($_REQUEST['type'] == 'validate')?'block':'none'?>">
<input type="checkbox" name="validate_store_session"  <?=($_REQUEST['validate_store_session'] OR $_REQUEST['type'] != 'validate')?' checked':''?> /> Store values in Session 
<input type="checkbox" name="type_filter"  <?=($_REQUEST['type_filter'] OR $_REQUEST['type'] != 'validate')?' checked':''?> /> Typecasting<br />
<input type="checkbox" name="individual_errors"  <?=($_REQUEST['individual_errors'])?' checked':''?> /> Individual field errors<br />
</div>

<div id="html_options" style="display:<?=($_REQUEST['type'] == 'html')?'block':'none'?>">
wrap with HTML tags <input type="text" name="custom_html_tags" style="width:270px" value="<?=htmlentities(stripslashes($_REQUEST['custom_html_tags']), ENT_QUOTES, $charset)?>" />
</div>

<div id="form_options" style="display:<?=($_REQUEST['type'] == 'form')?'block':'none'?>">
<input type="checkbox" name="form_full"  <?=($_REQUEST['form_full'])?' checked':''?> /> Full<br />
<input type="checkbox" name="form_field_errors"  <?=($_REQUEST['form_field_errors'])?' checked':''?> /> Errors below fields<br />
</div>

<div id="list_options" style="display:<?=($_REQUEST['type'] == 'list')?'block':'none'?>">
<input type="checkbox" name="list_search"  <?=($_REQUEST['list_search'])?' checked':''?> /> Search 
<input type="checkbox" name="list_sort"  <?=($_REQUEST['list_sort'])?' checked':''?> /> Sort
<input type="checkbox" name="list_paginator"  <?=($_REQUEST['list_paginator'])?' checked':''?> /> Paginator
<input type="checkbox" name="list_delete"  <?=($_REQUEST['list_delete'])?' checked':''?> /> Delete Icon
<input type="checkbox" name="list_ajax"  <?=($_REQUEST['list_ajax'])?' checked':''?> /> Delete, Sort and Filter with Ajax
<input type="checkbox" name="dynamic_list_header"  <?=($_REQUEST['dynamic_list_header'])?' checked':''?> /> Dynamic Header <br />
<input type="checkbox" name="list_memcache"  <?=($_REQUEST['list_memcache'])?' checked':''?> /> Memcache <br />
<input type="checkbox" name="list_odd_row"  <?=($_REQUEST['list_odd_row'])?' checked':''?> /> Different style for every second row <br />
<input type="checkbox" name="list_odd_column"  <?=($_REQUEST['list_odd_column'])?' checked':''?> /> Different style for every second column <br />
</div>

<br>
<a href="javascript:document.forms.red_button_form.submit();" style="outline:none" >
<img src="red_button.png" width="270" height="270" border="0" style="padding-left:270px" />
</a>
<input type="hidden" name="submitted" value="yes">
</form>
<?php
//endif;
if (!$_SESSION['array']) {
die();
} else {
$createTablePos = strpos($_SESSION['array'], 'CREATE TABLE');
if ($createTablePos === false) {
	$array = explode(',', $_SESSION['array']);
	$primary[] = $array[0];
} else {
	$tableStructure = stripslashes($_SESSION['array']);
	$tableNameStartPos = $createTablePos + 13;
	$tableNameEndPos = strpos($tableStructure, ' ', $tableNameStartPos + 1);
	$tableName = substr($tableStructure, $tableNameStartPos, $tableNameEndPos - $tableNameStartPos);
	$tableName = trim($tableName, '`');
	//echo 'Table ' . $tableName . ' evaluated<br>';
	$tableStructure = substr_replace($tableStructure, TMP_TABLE_NAME . $tableName . ' ', $tableNameStartPos, $tableNameEndPos - $tableNameStartPos);
	$tableStructure = str_replace('CREATE TABLE', 'CREATE TEMPORARY TABLE IF NOT EXISTS', $tableStructure);
	mysql_query($tableStructure);
	$sql = "DESCRIBE " . TMP_TABLE_NAME . $tableName;
	$result = mysql_query($sql);
	$i = 0;
	unset($array);
	while ($row = mysql_fetch_array($result)) {
		$array[$i] = $row['Field'];
		$nullable[$i] = ($row['Null'] == 'YES') ? true : false;
		$fullType[$i] = $row['Type'];
		$default[$i] = $row['Default'];
		$exactTypeStart = strpos($row['Type'], '(');
		if ($exactTypeStart) {
			$shortType[$i] = substr($row['Type'], 0, $exactTypeStart);
			$exactTypeStart = $exactTypeStart + 1;
			$exactTypeLength = strpos($row['Type'], ')') - $exactTypeStart;
			$exactType[$i] = substr($row['Type'], $exactTypeStart, $exactTypeLength);
		} else {
			$shortType[$i] = $row['Type'];
		}
		switch ($shortType[$i]) {
			case 'int':
				$simpleType[$i] = 'int';
				break;
			case 'tinyint':
				$simpleType[$i] = $exactType[$i] == '1' ? 'ckb' : 'int';
				//$simpleType[$i] = 'int';
				break;
			case 'enum':
				$simpleType[$i] = 'enum';
				break;
			case 'set':
				$simpleType[$i] = 'set';
				break;
			default:
				$simpleType[$i] = 'string';
		}
		if ($row['Key'] == 'PRI') {
			$primary[$i] = $row['Field'];
		}
		$i++;
	}
}
if (!is_array($array)) {
	echo 'Enter a valid comma separated list or a CREATE TABLE statement.<br>';
	die();
}

function varname($fieldname, $method = null, $context = null) {
	global $charset;
	$method = $method ? $method : $_REQUEST['http_method'];
	$custom_method_name = $custom_method_name ? $custom_method : $_REQUEST['custom_method_name'];
	$arbitrary_before_value = $arbitrary_before_value ? $arbitrary_before : $_REQUEST['arbitrary_before_value'];
	$arbitrary_after_value = $arbitrary_after_value ? $arbitrary_after : $_REQUEST['arbitrary_after_value'];
	$fieldname = htmlspecialchars($fieldname, ENT_QUOTES, $charset);
	switch ($method) {
	case 'custom':
		$varname = '$' . $_REQUEST['custom_http_method'] . '[\'' . $fieldname . '\']';
		break;
	case 'varname':
		$varname = '$' . $fieldname;
		break;
	case 'blank':
		$varname = $fieldname;
		$varContext = 'html';
		break;
	case 'blank_ucfirst':
		$varname = ucfirst($fieldname);
		$varContext = 'html';
		break;
	case 'valid':
		$varname = '$_VALID[\'' . $fieldname . '\']';
		break;
	case 'validdb':
		$varname = '$_VALIDDB[\'' . $fieldname . '\']';
		break;
	default:
		$varname = '$' . $method . '[\'' . $fieldname . '\']';
	}
	if ($_REQUEST['trim'])
		$s = 'trim(' . $varname . ')';
	else
		$s = $varname;
	if ($method == 'custom' OR $custom_method_name)
		$s = $custom_method_name . '(' . $s . ')';
	else
		$s = $s;
	if ($arbitrary_before_value)
		$s = stripslashes($arbitrary_before_value) . $s;
	if ($arbitrary_after_value)
		$s = $s . stripslashes($arbitrary_after_value);
	if ($context == 'html' AND $varContext != 'html') {
		$s = '<?php echo ' . $s . '?>';
	}
	return $s;
}

function trans($text, $context = null, $t = null, $transFunc = null) {
	if ($_REQUEST['trans']) {
            if (!$transFunc) {
                $transFunc = (WEBSITE == 'HROSE')?'ss':'__';
            }
		switch ($context) {
			case 'html':
				$s = '<?php echo '.$transFunc.'(\'' . $text . '\'' . (($t) ? ', \'' . $t . '\'' : '') . ')?>';
				break;
			case 'php':
				$s = $transFunc.'(\'' . $text . '\'' . (($t) ? ', \'' . $t . '\'' : '') . ')';
				break;
			default:
		 	$s = '\' . '.$transFunc.'(\'' . $text . '\'' . (($t) ? ', \'' . $t . '\'' : '') . ') . \'';
		}
	} else {
		switch ($context) {
			case 'php':
				$s = "'" . $text . "'";
				break;
			default:
				$s = $text;
		}
	}
	return $s;
}

// comma-separated list
if (!$tableStructure) {
	foreach ($array as $key => $value) {
		$value = str_replace(array('`', '\'', '"'), '', stripslashes($value));
		$value = trim($value);
		if (substr($value, 0, 2) == 'i ') {
			$simpleType[$key] = 'int';
			$array[$key] = substr($value, 2);
		} else if (substr($value, 0, 2) == 'b ') {
			$simpleType[$key] = 'ckb';
			$array[$key] = substr($value, 2);
		} else {
			$simpleType[$key] = 'string';
			$array[$key] = $value;
		}
	}
}

// blank list
$first = false;
foreach ($array as $key => $value) {
	if ($first) $blankList .= ', ';
	$blankList .= $value;
	$first = true;
}


// simple list
$first = false;
foreach ($array as $key => $value) {
	if ($first AND !$_REQUEST['arbitrary_before_value'] AND !$_REQUEST['arbitrary_after_value']) $simpleCode .= ', ' . chr(13);
	$simpleCode .= varname($value);
	$first = true;
}

require 'mysql_values.inc.php';
require 'validate.inc.php';
require 'html.inc.php';
require 'html_form.inc.php';
//require 'dojo_form.inc.php';
require 'html_list.inc.php';
//require 'dojo_list.inc.php';

switch ($_REQUEST['type']) {
	case 'simple':
		$code = $simpleCode;
		break;
		
	case 'mysql_values':
		$code = $mysqlValuesCode;
		break;
		
	case 'html':
		$code = $htmlCode;
		break;
		
	case 'form':
		$code = $htmlFormCode;
		break;
		
	case 'dojoform':
		$code = $dojoFormCode;
		break;
		
	case 'list':
		$code = $htmlListCode;
		break;
		
	case 'list2':
		$code = $htmlListCode2;
		break;
		
	case 'dojolist':
		$code = $dojoListCode;
		break;
		
	case 'validate':
		$code = $validateCode;
		break;
		
	default:
		$code = 'Welcome';
}
echo '<textarea>'.'<?
' . $code . '
?>'.'</textarea><br><br><div style="width:1000px">';
highlight_string('<? 
' . $code . ' 
?>');
echo '</div>';
}
?>
</body>
</html>